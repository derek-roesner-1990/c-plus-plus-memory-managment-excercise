/*************************************************************************************************************************************************
 * Filename - Name.cpp
 * Version - 1.1
 * Student - Derek Roesner
 * Student# - 040668227
 * C++ Programming - CST8219
 * Lab section - 400 
 * Assignment #2
 * Due - 24:00 23 Mar 2013 
 * Submission Made - 23 Mar 2013
 * Professor - Andrew Tyler 
 * Name.cpp contains the constructors to intialize and copy the custom name datatype.
 * Overloaded operator function for the assignment operator.
 * The destructor for the name datatype, to clear the memory for m_firstName and m_lastName.
 **************************************************************************************************************************************************/
#include "name.h"
#include <iostream>

//Default constructor
Name::Name():m_firstName(0),m_lastName(0){
}

//Overloaded constructor
Name::Name(char* fName, char* lName){
	
	if(fName != 0 && lName != 0){
	    int i;
		//allocating the memory needed to store the first and last names entered by the user.
		m_firstName = new char[strlen(fName) + 1];
		m_lastName = new char[strlen(lName) + 1];

		//copying the contents of the user input in the name object.
		for(i = 0; i < strlen(fName) + 1; i++){
			m_firstName[i] = fName[i];
		}

		for(i = 0; i < strlen(lName) + 1; i++){
			m_lastName[i] = lName[i];
		}
	}
}

//Copy constructor to copy information to empty Name objects.
Name::Name(Name& n){
	int i;
	
	if(n.GetFirstName() != 0 && n.GetLastName() != 0){

		m_firstName = new char[strlen(n.GetFirstName()) + 1];
		m_lastName = new char[strlen(n.GetLastName()) + 1];

		for(i = 0; i < strlen(n.GetFirstName()) + 1; i++){
			m_firstName[i] = n.m_firstName[i];
		}

		for(i = 0; i < strlen(n.GetLastName()) + 1; i++){
			m_lastName[i] = n.m_lastName[i];
		}
	}
}

//Overloaded assignment operator to assign Name information to the Name object on the left hand side.
Name& Name::operator=(Name &n){
	int i;

	if(this == &n){ //checking for self-assignment
		return *this;
	}

	if(n.GetFirstName() != 0 && n.GetLastName() != 0){

		m_firstName = new char[strlen(n.GetFirstName()) + 1];
		m_lastName = new char[strlen(n.GetLastName()) + 1];

		for(i = 0; i < strlen(n.GetFirstName()) + 1; i++){
			m_firstName[i] = n.m_firstName[i];
		}

		for(i = 0; i < strlen(n.GetLastName()) + 1; i++){
			m_lastName[i] = n.m_lastName[i];
		}
	}

	return *this;
}

//deconstructor for name object.
Name::~Name(){
	delete[] Name::m_firstName;
	delete[] Name::m_lastName;
}