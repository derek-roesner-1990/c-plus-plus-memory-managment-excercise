/*************************************************************************************************************************************************
 * Filename - Student.cpp
 * Version - 1.1
 * Student - Derek Roesner
 * Student# - 040668227
 * C++ Programming - CST8219
 * Lab section - 400 
 * Assignment #2
 * Due - 24:00 23 Mar 2013 
 * Submission Made - 23 Mar 2013
 * Professor - Andrew Tyler 
 * Student.cpp contains the constructors to create and copy student objects.
 * Overloaded operator functions for = , > , < and the == operator.
 * And the Report() method for outputing the contents of each Student object in a formatted way.
 **************************************************************************************************************************************************/
#include "student.h"
#include <iostream>
using namespace std;

//default constructor
Student::Student():m_id(0),m_mark(0),m_name(Name()){

}

//overloaded constructor
Student::Student(char* fName, char* lName, int id, float mark){
	m_name = Name(fName,lName);
	m_id = id;
	m_mark = mark;
}

//Overloaded assignment operator to assign the contents from student s to the student object on the left hand side.
Student& Student::operator=(Student &s){
	
		//checking for self-assignment
		if(this == &s){
			return *this;
		}
		m_id = s.GetID();
		m_mark = s.GetMark();
		//Overloaded assignment operator will be called to handle proper allocation of m_firstName and m_lastName within m_name.
		m_name = s.m_name;
		return *this;
}

//Copy constructor to copy information to empty Student objects.
Student::Student(Student& s){
	m_id = s.GetID();
	m_mark = s.GetMark();
	//Overloaded assignment operator will be called to handle proper allocation of m_firstName and m_lastName within m_name.
	m_name = s.m_name;
}

//Overloaded greater than operator.
bool Student::operator>(Student &s){
	return m_mark > s.GetMark();
}

//Overloaded less than operator.
bool Student::operator<(Student &s){
	return m_mark < s.GetMark();
}

//Overloaded equality operator.
bool Student::operator==(Student &s){
	return s.GetMark() == m_mark;
}


/*************************************************************************************************************************************************
*       Report();
*		Outputs the attributes of each student record to the console.
*       Version 1.0
*       Author - Derek Roesner     
***************************************************************************************************************************************************/
void Student::Report(){
	cout << Student::m_name.GetFirstName() << " " << Student::m_name.GetLastName()  << "\n" << "id = " << Student::m_id << "; mark = " << Student::m_mark << endl << endl;
}