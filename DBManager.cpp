/*************************************************************************************************************************************************
 * Filename - DBManager.cpp
 * Version - 1.1
 * Student - Derek Roesner
 * Student# - 040668227
 * C++ Programming - CST8219
 * Lab section - 400 
 * Assignment #2
 * Due - 24:00 23 Mar 2013 
 * Submission Made - 23 Mar 2013
 * Professor - Andrew Tyler 
 * DBManager.cpp contains the functions responsible for creating, copy and destroying the database.
 * and for managing a database of student records based on the contents of a text file.
 * ReadFile() to read each line of the file into an student object, 
 * ListStudents() to list the student objects created,
 * AddStudent() or DeleteRecordsByID() to add or delete an object from the array of students in memory. 
 * WriteFile() to write the contents back to disk once the user has closed the program.
 * OverwriteRecordByID() to overwrite the contents of a user selected record based on it's id value.
 * SelectRecordByID() to output the contents of a user selected record based on it's id value.
 * HighestMark() to output the contents of the student with the highest mark.
 * LowestMark() to ouput the contents of the student with the lowest mark.
 **************************************************************************************************************************************************/
#include <iostream>
#include <fstream>
#include <limits>
#include "DBManager.h"
#include "CoopStudent.h"
#include "RegularStudent.h"

using namespace std;
	
	//default constructor
	DBManager::DBManager(): m_ppRecords(NULL), m_numRecords(0), m_FileName(NULL){
	}

	//default desconstuctor
	DBManager::~DBManager(){
		int i;

		if(m_numRecords > 0){
			for(i = 0; i < m_numRecords; ++i){
				//calling the automatically created student object deconstuctor to deallocate the memory for each student.
				delete m_ppRecords[i];
			}
		}
		delete[] m_ppRecords;
		delete[] m_FileName;
	}

	//copy constructor for the DBManager object. 
	DBManager::DBManager(DBManager& db){
		int i;
		
		m_numRecords = db.m_numRecords;
		m_ppRecords = new Student*[m_numRecords];
		for(i = 0; i < m_numRecords; ++i){
			//allocating the memory for the proper type of student record based on the mark of the record to copy the information from.
			if(db.m_ppRecords[i]->GetMark() >= 80){
				m_ppRecords[i] = new CoopStudent(db.m_ppRecords[i]->GetName().GetFirstName(), db.m_ppRecords[i]->GetName().GetLastName(), db.m_ppRecords[i]->GetID(), db.m_ppRecords[i]->GetMark());
			}

			if(db.m_ppRecords[i]->GetMark() < 80){
				m_ppRecords[i] = new RegularStudent(db.m_ppRecords[i]->GetName().GetFirstName(), db.m_ppRecords[i]->GetName().GetLastName(), db.m_ppRecords[i]->GetID(), db.m_ppRecords[i]->GetMark());
			}
		}	

		//copying the contents of file name to the dbManager object.
		if(db.m_FileName != 0){
			m_FileName = new char[strlen(db.m_FileName)+1];
			for(i = 0; i < (int) strlen(db.m_FileName)+1; ++i){
				m_FileName[i] = db.m_FileName[i];
			}
		}
	}

	//overloaded [] operator for dbManager object.
	Student*& DBManager::operator[](int index){
			return m_ppRecords[index];
	}

/*************************************************************************************************************************************************
*	ReadFile()
*	Creates a filestream object from an user specified file.
*   For each line of the file that is found to contain the credentials
*	for a valid Regular or Coop Student object, an array of Student
*   pointers is allocated to point to the existing records plus the new
*   record and the empty space at the end of the array for the
*   new record is assigned the appropriate record based on the lines contents.
*	Version 1.2
*	Author- Derek Roesner      
***************************************************************************************************************************************************/
	void DBManager::ReadFile(void){
		int i, charAmt, id;
		float mark;
		char c;
		char* tempWord = 0;
		char* tempFirstName = 0;
		char* tempLastName = 0;
		Student** tempRecords;
		fstream file;
		
		
		cout << "OPEN FILE" << endl;
		cout << "Please enter the name of the file to open: ";
		charAmt = 0;
		while(cin.get(c)){
			
			//If input is invalid or the the newline character is read
			if(cin.bad() || c == '\n'){
				c = 0;
				m_FileName[charAmt] = '\0';
				charAmt = 0;
				break;
			}
			
			++charAmt;
			tempWord = m_FileName;
			//Extra space is made for the null termination character
			m_FileName = new char[charAmt+1];
			//Contents of tempWord copied back into the contents of m_FileName
			for(i=0; i<charAmt-1; ++i){
				m_FileName[i] = tempWord[i];
			}
			m_FileName[charAmt-1] = c;
			delete[] tempWord;
			tempWord = NULL;
			
		}

		if(m_FileName == NULL){
			cout << "Could not read filename";
		}

		//ios::_Nocreate will ensure that only existing files are opened.
		file.open(m_FileName, fstream::in , ios::_Nocreate);
		
		if(file.fail()){
			cout << "File doesn't exist - creating file\n" << endl;
			file.setstate(ios::goodbit);
			return;
		}

		cout << "READING RECORDS INTO MEMORY" << endl;

		tempRecords = NULL;
		c = 0;
		while(true){

			c = file.peek();

			//Checks for end of file 
			if(c == EOF){
				break;
			}

			//Checks for empty lines and lines only containing newline.
			if( c == ' '){
				while(c != '\n' && c != 'EOF'){
					file.get(c);
				}
				break;
			}

			//checking for the newline character.
			if(c == '\n' ) {
				file.get(c);
				continue;
			}

			//Reading the m_firstName from file
			while(file.get(c))
			{

				if( c == ' '){
					tempWord = NULL;
					break;
				}

				++charAmt;
				tempWord = tempFirstName;
				// 1 is added to charAmt to make the extra space needed for the string termination character
				tempFirstName = new char[charAmt+1];
				
				//charAmt-1 is used to keep space for the new character to be added to the end of the allocated memory
				for(i = 0; i < charAmt-1; ++i){
					//Copying the old contents of the current m_ppRecords.m_FileName to the new location of the current m_ppRecords.m_FileName.
					tempFirstName[i] = tempWord[i];
				}
				//Adding the character read from file to m_firstName.
				tempFirstName[charAmt-1] = c;
				delete[] tempWord;
			}
			//Adding the string termination character to the m_firstName.
			if(tempFirstName != NULL){
				tempFirstName[charAmt] = '\0';
			}

			//if the file is empty
			if(m_numRecords == 1 && c == 0){
				//reset the count of student records
				m_numRecords = 0;
				break;
			}

		charAmt = 0;
		//Reading the m_lastName from file
		while(file.get(c)){
			
			if( c == ' '){
				tempWord = NULL;
				break;
			}

			//Same algorithm as reading m_firstName from file.
			++charAmt;
			tempWord =  tempLastName;
			tempLastName = new char[charAmt+1];
			
			//charAmt-1 is used to keep space for the new character that is to be added to the end of the allocated memory
			for(i = 0; i < charAmt-1; ++i){
				 tempLastName[i] = tempWord[i];
			}
			tempLastName[charAmt-1] = c; 
			delete[] tempWord;
		}

		if(tempLastName != NULL){
			tempLastName[charAmt] = '\0';
		}

		charAmt = 0;

		//Reading the student id from file
		file >>	id; 
		
		//Reading the mark from file
		file >> mark;

		//Expanding the memory for the additional student record
		++m_numRecords;
		tempRecords = m_ppRecords; //tempRecords now points to the same memory as m_ppRecords.
		m_ppRecords = new Student*[m_numRecords];

			//m_numRecords-1 is used to keep space for the new character to be added to the end of the allocated memory
			for(i = 0; i < m_numRecords-1; ++i){
				/*
					The memory previously pointed to by m_ppRecords is copied back into the contents of
					m_ppRecords and m_ppRecords.m_name using the overloaded copy constructor for Student objects.
			    */
				m_ppRecords[i] = tempRecords[i];
			}

			//Clears the old contents of m_ppRecords from memory 
			delete[] tempRecords;
			tempRecords = NULL;
			//the contents of the pointers within oldRecords cannot be deallocated in order to maintain the integrity of the new memory assigned to m_ppRecords.

		if(mark >= 80.0){
			m_ppRecords[m_numRecords-1] =  new CoopStudent(tempFirstName,tempLastName,id,mark);
		}

		if(mark < 80.0){
		    m_ppRecords[m_numRecords-1] =  new RegularStudent(tempFirstName,tempLastName,id,mark);
		}
	
		//At the end of each line I still need to consume the newline character.
		//Must also detect the eof() character in case the record is on the last line.
		while(c != '\n' && file.eof() == false){
			file.get(c);
		}
	}
	delete[] tempFirstName;
	delete[] tempLastName;
	cout << "There are " <<  m_numRecords  << " student records." << endl;
	file.close();
	cout << '\n';
	}
/*************************************************************************************************************************************************
*       ListStudents()
*		Moves through the memory pointed to by m_ppRecords and calls the Report method of
*		each student pointer to output the atttibutes of each Coop or Regular Student object.
*       Version 1.1
*       Author - Derek Roesner     
*************************************************************************************************************************************************/
void DBManager::ListRecords(void){
	/*
		Loop through the list of student records and call the virtual report method of each student.
	*/
	int i;
	cout << "LIST RECORDS IN MEMORY" << endl;
	if(m_numRecords == 0){
		cout << "No records" << endl;
	}else{
		for(i = 0; i < m_numRecords; ++i){
			cout << "Student #" << i+1 << endl; 
			m_ppRecords[i]->Report();
		}
	}
}
/*************************************************************************************************************************************************
*       AddStudent()
*		Resizes the space of memory pointed to by m_ppRecords to make space for an additonal record.
* 		Reads user input for the new record and places each attribute into its appropriate data member within 
*		either a Coop Student object or Regular Student object that is pointed to by a student pointer.
*       Version 1.2
*       Author - Derek Roesner     
*************************************************************************************************************************************************/
void DBManager::AddRecord(void){
		
	int i, id, reading, charAmt;
	float mark;
	char c;
	char* tempWord;
	char* tempFirstName = 0;
	char* tempLastName = 0;
	Student** oldRecords;

	//oldRecords now points to the same memory as m_ppRecords.
	oldRecords = m_ppRecords;
	++m_numRecords;
	//m_ppRecords now points to a new memory location expanded for one extra pointer to a student record object.
	m_ppRecords = new Student*[m_numRecords];

	//m_numRecords-1 is used to keep space for the new character that is to be added to the end of the allocated memory
	for(i = 0; i < m_numRecords-1; ++i){
			
		/*the memory previously pointed to by m_ppRecords is copied back into the contents
		of m_ppRecords using the student objects and it's names copy constructors*/
		m_ppRecords[i] = oldRecords[i];
	}

	//Clears the old contents of m_ppRecords from memory 
	delete[] oldRecords;
	oldRecords = NULL;
	//the contents of the pointers within oldRecords cannot be deallocated in order to maintain the integrity of the new memory assigned to m_ppRecords.
	charAmt = 0;
	cin.get();
	reading = 1;
	while(reading == 1){

		cout << "Please enter first name: ";
			
		while(cin.get(c)){
			//Exits loop at end of user input
			if(c == '\n'){
				reading = 0;
				break;
			}

			if(c == ' '){
				cout << "First name cannot contain a space" << endl;
				delete[] tempFirstName;
				tempFirstName = 0;
				charAmt = 0;
					
				c = 0;
				while(c != '\n'){
					cin.get(c);
				}

				break;
			}

			++charAmt;
			tempWord = tempFirstName;
			//the memory pointed to by the m_firstName variable of the current record is expanded for an extra character plus the string termination character.
			tempFirstName = new char[charAmt+1];

			for(i = 0; i < charAmt-1; ++i){
				tempFirstName[i] = tempWord[i];
			}

			tempFirstName[charAmt-1] = c;
			delete[] tempWord;
		}
		
		if(tempFirstName != NULL){
			tempFirstName[charAmt] = '\0'; 
		}
	}

	charAmt = 0;
	reading = 1;
	while(reading == 1){
		cout << "Please enter last name: ";
		while(cin.get(c)){

			if(c == '\n'){
				reading = 0;
				break;
			}

			if(c == ' '){
				cout << "Last name cannot contain a space" << endl;
				delete[] tempLastName;
				tempLastName = NULL;
				charAmt = 0;

				c = 0;
				while(c != '\n'){
					cin.get(c);
				}
				break;
			}

			++charAmt;
			tempWord = tempLastName;
			tempLastName = new char[charAmt+1];
			for(i = 0; i < charAmt-1; ++i){
				 tempLastName[i] = tempWord[i];
			}

			tempLastName[charAmt-1] = c;
			delete[] tempWord;
			}
			if(tempLastName != NULL){
				 tempLastName[charAmt] = '\0'; 
			}
	}
	
	
	while(1){
		cout << "Please enter id: ";
		//cin automatically converts user input into a integer for id.
		cin >> id;
		if(cin.fail()){
			cout << "INVALID INPUT" << endl;
			cin.clear();
			cin.ignore(std::numeric_limits<int>::max(),'\n');
			continue;
		}
		break;
	}
		
	//the user will be prompted for input until valid input is recieved.
	while(1){
		cout << "Please enter mark: ";
		cin >> mark;
			
		if(cin.fail()){
			cout << "INVALID INPUT" << endl;
			cin.clear();
			cin.ignore(std::numeric_limits<int>::max(),'\n');
			continue;
		}

		if(mark < 0 || mark > 100){
			cout << "Mark must be within the range of 0 to 100." << endl; 
			continue;
		}

		break;
	}

	/*
	  Creating either a new CoopStudent ot Regular student based on the mark entered, then assigning
	  this object to the pointer at the desired position within m_ppRecords allocated memory.
	*/

	if(mark >= 80){
		m_ppRecords[m_numRecords-1] = new CoopStudent(tempFirstName,tempLastName,id,mark);
	}

	if(mark < 80){
		m_ppRecords[m_numRecords-1] = new RegularStudent(tempFirstName,tempLastName,id,mark);
	}

	//deallocating the memory assigned to tempFirstName and tempLastName.
	delete[] tempFirstName;
	delete[] tempLastName;

	cout << "Number of Records = " << m_numRecords << endl;
	cout << "ADDED A NEW RECORD IN MEMORY OK" << endl;
	cout << '\n';
}


/*************************************************************************************************************************************************
*       DeleteRecordByID();
*		If at least a record exists in the database, this method	
*		attempts to retrieve the record with the id value 
*		that matches the value entered by the user. 
*		If found, the record is removed from the database 
*		and the array of student record pointers is rebuilt. 
*       Version 1.1
*       Author - Derek Roesner     
***************************************************************************************************************************************************/
	void DBManager::DeleteRecordByID(void){

		Student** oldRecords;
		int i,r,idToFind,idFound=0, deletePos,emptyElementOffset=0;
		char c = 0;

		if(m_numRecords== 0){
			cout << "No records" << endl;
			return;
		}

		/*Accepting the user input to choose a id to
		  match to one of the records within m_ppRecords.*/
		cout << "m_numRecords = " << m_numRecords << endl;
		while(1){
			cout<<"Please enter the id of the record to delete: ";
			cin >> idToFind;
			//If entry is unsuccessful.
			if(cin.fail() == true){
				cin.clear();
				cin.ignore(std::numeric_limits<int>::max(),'\n');
				cout<<"Invalid id - Press 1 to retry: ";
				cin>>r;
				cout<< "\n";

				if(r == 1){
					continue;
				}else{
					return;
				}
			}
			//if entry isn't 1, exit the function.
			break;
		}

		for(i = 0 ; i < m_numRecords; ++i){
			if(m_ppRecords[i]->GetID() == idToFind){
				idFound = 1;
				cout << "Found record to delete." << endl;
				//marks the position of the deleted element to avoid copying the contents at that element when resizing the array of records.
				deletePos = i;

				oldRecords = m_ppRecords;
				m_ppRecords = new Student*[m_numRecords-1];
		
				//same copying algorithm is used as when the space pointed to by m_ppRecords is expanded in ReadFile()
				for(i = 0; i < m_numRecords; ++i){
					if(i != deletePos){
						//The code for the overloaded assignment operator function is used for the assignment of the student record and it's name.
						m_ppRecords[i-emptyElementOffset] = oldRecords[i];
					}else{
						delete oldRecords[i];
						emptyElementOffset=1;
					}
				}//end of for loop 	
			}//end of if statement checking to see if the desired id matches the current record.
		}//end of for loop searching for a match with the desired id. 

		//Checking to see if the id entered by the user was found in a record within the database.
		if(idFound == 1){
			//Clears the old contents of m_ppRecords from memory
			delete[] oldRecords;
			oldRecords = NULL;

			--m_numRecords;
			cout << "Student record deleted" << endl;
			cout << "Number of Records = " << m_numRecords << endl;
			cout << '\n';
		}else{
			cout << "Unable to find a record with id# " << idToFind <<"\n" << endl;
		}

	}
/*************************************************************************************************************************************************
*       WriteFile();
*		Re-opens the user specified file in write mode.
*		Moves through the array of student pointers while writing the attributes of 
*		each Coop or Regular Student record to a line in the file they were read from.
*       Version 1.1
*       Author - Derek Roesner     
***************************************************************************************************************************************************/
void DBManager::WriteFile(void){
	int i;
	char c;
	fstream file;

	//if an existing file was not found in ReadFile(), the file is created using the file name entered by the user.
	file.open(m_FileName,fstream::out);
	//Each student record has it's attributes wrote to file on a single line.
	for(i = 0; i < m_numRecords; ++i){
		file << m_ppRecords[i]->GetName().GetFirstName() << " " << m_ppRecords[i]->GetName().GetLastName() << " " << m_ppRecords[i]->GetID() << " " << m_ppRecords[i]->GetMark() << endl;
	}

	file.close();
	//The memory pointer to by the student pointers within m_ppRecords and m_ppRecords itself is deallocated by the default deconstuctor when the execution exits main.
}

/*************************************************************************************************************************************************
*       OverwriteRecordbyID();
*		If at least a record exists in the database, this method	
*		attempts to retrieve the record with the id value 
*		that matches the value entered by the user. 
*		If found, the Coop or Regular student record 
*		has it's information overwritten by the user input.
*       Version 1.1
*       Author - Derek Roesner     
***************************************************************************************************************************************************/
void DBManager::OverwriteRecordByID(){

	int i, j, idToFind, response, charAmt, match = 0, id;
	float mark;
	char c = 0;
	Student** tempRecords = 0;
	char* tempWord = 0;
	char* tempFirstName = 0;
	char* tempLastName = 0;
		
	if(m_numRecords==0)
	{
		cout<<"No records"<<endl;
		return;
	}

		while(1){
			cout<<"Please enter the id of the student to overwrite: ";
			cin >> idToFind;
			//If entry is non numeric or unsuccessful.
			if(cin.fail() == true){
				cin.clear();
				cin.ignore(std::numeric_limits<int>::max(),'\n');
				//press 1 to retry entry
				cout<<"Invalid ID - Press 1 to retry: ";
				cin>>response;
				cout<< "\n";

				if(response == 1){
					continue;
				}else{
					return;
				}
			}
			break;
		}

		for(i = 0; i < m_numRecords; ++i){
			if(idToFind == m_ppRecords[i]->GetID()){
				
				match = 1;
				//pos = i
				cout << "Student record found" << endl;
				
				//if the record is found, the memory must be resized to adjust for the change to m_ppRecords.
				tempRecords = m_ppRecords;
				m_ppRecords = new Student*[m_numRecords];

				for(j = 0; j < m_numRecords; ++j){
					//if the record isn't the record to delete, it's contents are copied back into the new memory allocated to m_ppRecords.
					if(j != i){
						m_ppRecords[j] = tempRecords[j];
					}else{
					//if found contents of the student record pointed to by the the pointer at tempRecords[j] is deallocated.
						delete tempRecords[j];
					}
				}
				delete[] tempRecords;

				while(c != '\n'){
					cin.get(c);
				}
				//same algorithm as in addRecord(), for creating a new student record from user input.
				while(1){
					charAmt = 0;
					cout << "Please enter the new first name : ";
					
					while(cin.get(c)){
			
						//Exits loop at end of the line
						if(c == '\n'){
							break;
						}

						if(c == ' '){
							cout << "First name cannot contain a space" << endl;
							delete[] tempFirstName;
							tempFirstName = 0;
							charAmt = 0;
					
							c = 0;
							while(c != '\n'){
								cin.get(c);
							}
							continue;
						}

						++charAmt;
						tempWord = tempFirstName;

						tempFirstName = new char[charAmt+1];

						for(j = 0; j < charAmt-1; ++j){
							tempFirstName[j] = tempWord[j];
						}

						tempFirstName[charAmt-1] = c;
						delete[] tempWord;
					}
		
					if(tempFirstName != NULL){
						tempFirstName[charAmt] = '\0'; 
					}
					break;
				}
			

				while(1){ 
					charAmt = 0;
					cout << "Please enter the new last name : ";
					while(cin.get(c)){

						if(c == '\n'){
							break;
						}

						if(c == ' '){
							cout << "Last name cannot contain a space" << endl;
							delete[] tempLastName;
							tempLastName = 0;
							charAmt = 0;

							c = 0;
							while(c != '\n'){
								cin.get(c);
							}
							continue;
						}

						++charAmt;
						tempWord = tempLastName;
						tempLastName = new char[charAmt+1];
						for(j = 0; j < charAmt-1; ++j){
							tempLastName[j] = tempWord[j];
						}

						tempLastName[charAmt-1] = c;
						delete[] tempWord;
					}
					if(tempLastName != NULL){
						tempLastName[charAmt] = '\0'; 
					}
					break;
				}

				while(1){
					cout << "Please enter the new id: ";
					cin >> id;
					if(cin.fail()){
						cout << "INVALID INPUT" << endl;
						cin.ignore(std::numeric_limits<int>::max(),'\n');
						continue;
					}
					break;
				}
		
				//the user will be prompted for input until valid input is recieved.
				while(1){
					cout << "Please enter the new mark: ";
					cin >> mark;
			
					if(cin.fail()){
						cout << "INVALID INPUT" << endl;
						cin.ignore(std::numeric_limits<int>::max(),'\n');
						continue;
					}

					if(mark < 0 || mark > 100){
						cout << "Mark must be within the range of 0 to 100." << endl; 
						continue;
					}
					break;
				}

	  
			/*
			 Creating either a new CoopStudent ot Regular student based on the mark entered, then assigning
			 this object to the pointer at the desired position within m_ppRecords allocated memory.
			*/
				if(mark >= 80){
					m_ppRecords[i] = new CoopStudent(tempFirstName,tempLastName,id,mark);
				}

				if(mark < 80){
					m_ppRecords[i] =  new RegularStudent(tempFirstName,tempLastName,id,mark);
				}

				cout << "Student record overwritten\n" << endl;
				break;
			}//end of if statement checking to see if the id of the current record matches the chosen id.
		}//end of for loop, moving through the student records.

		//deallocating the memory assigned to tempFirstName and tempLastName.
		delete[] tempFirstName;
		delete[] tempLastName;
		tempFirstName = 0;
		tempLastName = 0;

		if(match == 0){
				cout << "Unable to find a record with id# " << idToFind <<"\n" << endl;
		}
}

/*************************************************************************************************************************************************
*       SelectRecordByID();
*		If at least a record exists in the database, this method		
*		attempts to retrieve the record with the id value 
*		that matches the value entered by the user. 
*		If found, the Coop or Regular Student record has it's information
*		output using the Report() function accessed by the student pointer.
*       Version 1.1
*       Author - Derek Roesner     
***************************************************************************************************************************************************/
	void DBManager::SelectRecordByID(){
		int i, idToFind, response;
		char c = 0;
		
		if(m_numRecords==0)
		{
			cout<<"No records"<<endl;
			return;
		}
		else
		{
			//Accepts input from the user, until a valid ID is found.
			while(1){
				cout<<"Please enter the id of the student to search for: ";
				cin >> idToFind;
				//If the entry is unsuccessful.
				if(cin.fail() == true){
					cin.clear();
					cin.ignore(std::numeric_limits<int>::max(),'\n');
					cout<<"Invalid ID - Press 1 to retry: ";
					cin>>response;
					cout<< "\n";
					//press 1 to retry entry
					if(response == 1){
						continue;
					}else{
						return;
					}
				}
				//if entry isn't 1, exit the function.
				break;
			}

			for(i = 0; i < m_numRecords; ++i){
				/*
				  If a record is found that has a id value that matches the value entered by the user,
				  it's contents are output to the console screen by calling student records virtual report function. 
				*/
				if(idToFind == m_ppRecords[i]->GetID()){
					m_ppRecords[i]->Report();
					cout << endl;
					return;
				}
			}
			cout << "Unable to find a record with id# " << idToFind << endl;
		}
}

/*************************************************************************************************************************************************
*       HighestMark()	
*	    If at least a record exists in the database, this method
*       loops through the student pointers within m_ppRecords and finds
*       the record with the highest mark. Once found, the contents
*		of the Coop or Regular student record are printed to the console.
*       Version 1.1
*       Author - Derek Roesner     
***************************************************************************************************************************************************/
void DBManager::HighestMark(){
		
	if(m_numRecords==0){
		cout<<"No records"<<endl;
		return;
	}
	else
	{
		DBManager DBM = *this;
		Student* highest = DBM[0];

		for(int i=0;i<m_numRecords;++i)
			if(*(DBM[i])>*highest)
				highest=DBM[i];
			
		cout<<"Student with highest mark is:" << endl;
		highest->Report();
	}
}

/*************************************************************************************************************************************************
*       LowestMark()	
*	    If at least a record exists in the database, this method
*       loops through the student pointers within m_ppRecords and finds
*       the record with the lowest mark. Once found, the contents
*		of the Coop or Regular student record are printed to the console.
*       Version 1.1
*       Author - Derek Roesner     
***************************************************************************************************************************************************/
void DBManager::LowestMark(){
		if(m_numRecords==0)
		{
			cout<<"No records"<<endl;
			return;
		}
		else
		{
			//Uses DBM copy constructor since the assignment operator is not overwritten
			DBManager DBM = *this;
			//uses overloaded assignement operator to create student object.
			Student* lowest = DBM[0];

			for(int i=0; i<m_numRecords;++i)
				//uses overloaded < operator to make a comparison on the mark of the student objects.
				if(*(DBM[i])<*lowest)
					//Uses the overloaded assignment operator to create the new student from the contents of DBM[i].
					lowest=DBM[i];
				
			cout<<"Student with lowest mark is:"<<endl;
			lowest->Report();
		}

	}